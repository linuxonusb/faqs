# FAQs



## Q: What is "Linux on USB"?

*A:* It is a Linux system installed on a USB flash drive.

How many people buy a Windows install CD and instll it themselves? Not many.
The vast majority of people just buy a personal computer with Windows or Mac operating system already installed.
Why is Linux different?
For Linux, why does a user have to install the system themselves?
Why?

When you want to drive a car, you buy a car.
You don't buy parts and assemble them yourself.
Why should using Linux be any differnt?
When you want to use a Linux system, you should just buy a Linux system installed on a computer (or, on some hardware).
You don't have to get an install disc or install falsh drive and install the operating system yourself.


Linux on USB provides a solution.
It's a Linux system pre-installed on a USB drive.
It's really a "virtual" computer.
It is not a computer (with CPU and memory, etc.).
But, it is just like a computer when booted from a host computer.





## Q: Who is it for?

*A:* Linux on USB is for anyone _who wants to drive a car without assembling one._

Whether you are an experienced Linux user and a person who is just starting with Linux,
Linux on USB will be a perfect alternative to "making your own car".



## Q: What are the use cases?

*A:* There can be many.

Do you carry a laptop to work because you need to use the same computer both at home and at work?
No need. Just carry this Linux on USB flash drive and you can work on the "same computer" anywhere you go.

Are you visiting your uncle's house over the weekend? And, thinking about carrying your laptop?
You don't have to. Just take this Linux on USB drive and borrow your uncle's computer. 
You can use "your own computer" even at your uncle's house.

Do you need many computers? You may not have to use all of them at the same time,
but you still need many different computer for different purposes.
Why spend thousands of dollars buying all these computers when you only use them occasionally.
You can have as many Linux on USBs with a fraction of the cost.
Each Linux on USB drive is a separate computer.





## Q: What do I need to get started?

*A:* Not much.

Linux on USB is a complete system, a "virtual computer".
All you need is a computer (a real physical computer with CPU and memory, etc.) that can be booted from USB.




## Q: What is included in "Linux on USB"?


A brand-new premium brand USB flash drive.
A Linux system installed on the flash drive.





## Q: Ubuntu 16.04 LTS vs Ubuntu 17.04

Companies, especially large companies, want stability.
Stability over a long period of time (say, years) triumphs over everything else.
They are not particularly intestred in new features and what not.

For this type of users, Canonical (the company behind Ubuntu distribution) 
promises to support certain releases over a long period of time, in terms of maintenance and bug patches, etc. 
They call it these releses LTS (long term support).
The most recent LTS release is Ubunt Desktop and Server 16.04 LTS.

On the other hand, some users value the ability to try new features
and the most up-to-date system kernels, etc.
For those types of users, the most recent release is more suitable.
AS of this writing, the newest release is 17.04 for both Desktop and Server.









